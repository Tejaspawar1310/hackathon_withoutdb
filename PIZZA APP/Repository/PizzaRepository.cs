﻿using PIZZA_APP.Exceptons;
using PIZZA_APP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIZZA_APP.Repository
{
    #region PIZZA REPOSITORY
    internal class PizzaRepository:IPizzaInterface
    {
        List<Pizza> pizzaItems;
        List<AvailableLocations> availableLocations;
        List<Pizza> pizzaCart = new List<Pizza>();

        public PizzaRepository()
        {
            pizzaItems = new List<Pizza>()
            {
                new Pizza(){PizzaId=1,Price=200,PizzaName="Peppy Paneer"},
                new Pizza(){PizzaId=2,Price=400,PizzaName="Mexican Green Wave"},
                new Pizza(){PizzaId=3,Price=600,PizzaName="Cheese Corn"},
                new Pizza(){PizzaId=4,Price=800,PizzaName="Italian Special"},
                new Pizza(){PizzaId=5,Price=1000,PizzaName="Four Cheese"}
            };
            availableLocations = new List<AvailableLocations>()
            {
                new AvailableLocations(){PinCode=400048},
                new AvailableLocations(){PinCode=400058},
                new AvailableLocations(){PinCode=400068},
                new AvailableLocations(){PinCode=400078},
                new AvailableLocations(){PinCode=400088}
            };

        }
        #region ADDPIZZATOCART
        public void AddPizzaToCart(string pizzaId)
        {
            foreach (Pizza item in pizzaItems)
            {
                if (item.PizzaId == int.Parse(pizzaId))
                {
                    pizzaCart.Add(item);
                    Console.WriteLine("Your Pizza has been added to cart");
                    return;
                }
            }
            Console.WriteLine("The pizza is not present  ");

        }
        #endregion

        #region paybill
        public void PayBill(string userName, int pinCode)
        {
            if (pizzaCart.Count() == 0)
            {
                Console.WriteLine("Cart is Empty, Please add Pizza");
                return;
            }
            int totalAmount = 0;
            foreach (Pizza item in pizzaCart)
            {
                totalAmount += item.Price;
            }
            PrintTotalBill(totalAmount, userName, pinCode);
            pizzaCart.Clear();

        }
        #endregion

        #region PRINT BILL
        private void PrintTotalBill(int totalAmount, string userName, int pinCode)
        {
            Console.WriteLine("++++++++++++++++++++");
            Console.WriteLine("        Invoice     ");
            Console.WriteLine("++++++++++++++++++++");
            Console.WriteLine($"Name: {userName}");
            Console.WriteLine($"PinCode: {pinCode}");
            Console.WriteLine("                   ");
            foreach (Pizza item in pizzaCart)
            {
                Console.WriteLine($" {item.PizzaName} {item.Price}");
            }
            Console.WriteLine($"\n Total Bill Amount: {totalAmount}Rs.  \n");
            Console.WriteLine($" Date:{DateTime.Now.ToString()}\n");
            Console.WriteLine("---------");

        }
        #endregion
        #region RemoveFromCart
        public void RemovePizzaFromCart(string pizzaId)
        {

            try
            {
                if (pizzaCart.Count == 0)
                {
                    throw new CartEmptyException("Oops your Cart is Empty, Can't be deleted");
                    return;
                }

                foreach (Pizza item in pizzaCart)
                {
                    if (item.PizzaId == int.Parse(pizzaId))
                    {
                        pizzaCart.Remove(item);
                        return;
                    }
                }
                throw new PizzaDeliveryNotAvailableException("Pizza is not Present in the Cart");
            }
            catch (PizzaDeliveryNotAvailableException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (CartEmptyException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion

        #region Location Pincode
        public bool AvailablePinCode(int pinCode)
        {
            var matchPin = availableLocations.Where(y => y.PinCode == pinCode).FirstOrDefault();
            if (matchPin != null)
            {
                return true;
            }
            return false;
        }
        #endregion

        public void UpdatePizzaToCart(int pizzaId)
        {
            throw new NotImplementedException();
        }

        #region ViewCart
        public void ViewCart()
        {
            Console.WriteLine("--Your Cart--");
            if (pizzaCart.Count == 0)
            {
                Console.WriteLine("Your Cart is Empty");
            }
            else
            {
                //ViewCart();
                //Console.WriteLine("Enter the Pizza Id to delete from the Cart:");
                //string pizzaId = Console.ReadLine();
                //RemovePizzaFromCart(pizzaId);
                //PizzaRepository.ViewCart();
                foreach (Pizza item in pizzaCart)
                {
                    Console.WriteLine(item);
                }
            }
        }
        #endregion
        public void ShowMenu()
        {
            foreach (Pizza item in pizzaItems)
            {
                Console.WriteLine(item);
            }
        }
    }
    #endregion
}

