﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIZZA_APP.Repository
{
    internal interface IPizzaInterface
    {

        public void AddPizzaToCart(string pizzaId);

        public void RemovePizzaFromCart(string pizzaId);

        public void UpdatePizzaToCart(int pizzaId);

        public void ViewCart();

        public void PayBill(string userName, int pinCode);

    }
}
