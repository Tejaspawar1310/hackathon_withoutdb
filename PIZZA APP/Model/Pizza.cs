﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIZZA_APP.Model
{
    internal class Pizza
    {
        public int PizzaId { get; set; }
        public string PizzaName { get; set; }
        
        public int Price { get; set; }

        public override string ToString()
        {
            return $"{PizzaId}\t{PizzaName}\t{Price}";
        }

    }
}
