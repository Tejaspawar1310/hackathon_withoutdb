﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIZZA_APP.Exceptons
{
    internal class CartEmptyException : Exception
    {
        public CartEmptyException()
        {

        }
        public CartEmptyException(string message) : base(message)
        {

        }
    }
}
