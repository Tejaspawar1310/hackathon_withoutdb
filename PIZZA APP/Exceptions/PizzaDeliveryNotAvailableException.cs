﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIZZA_APP.Exceptons
{
    internal class PizzaDeliveryNotAvailableException:Exception
    {
        public PizzaDeliveryNotAvailableException()
        {

        }
        public PizzaDeliveryNotAvailableException(string message) : base(message)
        {

        }

    }
}
