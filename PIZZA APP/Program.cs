﻿// See https://aka.ms/new-console-template for more information

using PIZZA_APP.Repository;

Console.WriteLine("Enter Username:");
string userName = Console.ReadLine();

Console.WriteLine($"==========Welcome To Our Pizza Shop: {userName}==========");

ShowPinCode:
Console.WriteLine("Enter your PinCode:");
int pinCode = int.Parse(Console.ReadLine());

PizzaRepository PizzaRepository = new PizzaRepository();

//Console.WriteLine($"{userName} Enter Your Choice:");

if (!PizzaRepository.AvailablePinCode(pinCode))
{
    Console.WriteLine("\nOhhh no we are so sorry \n We are currently available only in this location: 400048,400058,400068,400078,400088");
    Console.WriteLine("Do you want to continue:: Y or N");
    string decide = Console.ReadLine();
    if (decide.ToLower() == "y")
    {
        goto ShowPinCode;
    }
    else
    {
        Environment.Exit(0);
    }
}
else
{
    Console.WriteLine("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    Console.WriteLine("|View Pizza:1 || View Cart:2 ||Remove Cart:3 || Pay Bill:4 || Exit:5 ||");
    Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

    string choice = Console.ReadLine();
    
    while (choice != "5")
    {
        if (choice == "1")
        {
            PizzaRepository.ShowMenu();
            Console.WriteLine("Enter the Pizza Id to add Pizza to cart");
            string pizzaId = Console.ReadLine();
            PizzaRepository.AddPizzaToCart(pizzaId);
            PizzaRepository.ViewCart();

        }
        else if (choice == "2")
        {
            PizzaRepository.ViewCart();
        }

        else if (choice == "3")
        {
            PizzaRepository.ViewCart();
            Console.WriteLine("Enter the Pizza Id to delete from the Cart:");
            string pizzaId = Console.ReadLine();
            PizzaRepository.RemovePizzaFromCart(pizzaId);
            PizzaRepository.ViewCart();
        }
        else if (choice == "4")
        {
            PizzaRepository.PayBill(userName, pinCode);
        }

        Console.WriteLine("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        Console.WriteLine("|View Pizza:1 || View Cart:2 ||Remove Cart:3 || Pay Bill:4 || Exit:5 ||");
        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        choice = Console.ReadLine();

    }
}

